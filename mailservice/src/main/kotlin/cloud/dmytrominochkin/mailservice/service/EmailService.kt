package cloud.dmytrominochkin.mailservice.service

import cloud.dmytrominochkin.mailservice.model.EmailContent
import org.springframework.beans.factory.annotation.Value
import org.springframework.mail.MailException
import org.springframework.mail.javamail.JavaMailSender
import org.springframework.mail.javamail.MimeMessageHelper
import org.springframework.mail.javamail.MimeMessagePreparator
import org.springframework.stereotype.Service

@Service
class EmailService(
    @Value("\${spring.mail.addresses.from}") private val emailFromAddress: String,
    @Value("\${spring.mail.addresses.replyTo}") private val emailReplyToAddress: String,
    private val emailSender: JavaMailSender
) {
    @Throws(MailException::class)
    fun sendEmail(recipient: String, subject: String, content: EmailContent) {
        val messagePreparator = MimeMessagePreparator { mimeMessage ->
            val messageHelper = MimeMessageHelper(mimeMessage, true)
            messageHelper.apply {
                setFrom(emailFromAddress)
                setReplyTo(emailReplyToAddress)
                setTo(recipient)
                setSubject(subject)
                setText(content.text, content.html)
            }
        }
        emailSender.send(messagePreparator)
    }
}