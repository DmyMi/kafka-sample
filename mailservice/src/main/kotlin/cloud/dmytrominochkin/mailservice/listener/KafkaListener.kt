package cloud.dmytrominochkin.mailservice.listener

import cloud.dmytrominochkin.mailservice.model.User
import cloud.dmytrominochkin.mailservice.service.EmailService
import cloud.dmytrominochkin.mailservice.service.TemplateService
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component
import org.springframework.kafka.annotation.KafkaListener
import org.springframework.mail.MailException

@Component
class KafkaListener(
    private val emailService: EmailService,
    private val templateService: TemplateService
) {
    @KafkaListener(topicPattern = "\${kafka.topics.user-registration}", autoStartup = "\${kafka.enabled}")
    fun listenToRegistration(record: ConsumerRecord<String, User>) {
        logger.info("Request for project status change received: $record")

        val payload = record.value()

        if (payload.email.isBlank()) {
            logger.warn("Ignoring request to send an e-mail without e-mail address: $record")
            return
        }

        try {
            emailService.sendEmail(
                payload.email,
                "Welcome!",
                templateService.generateEmailContent(payload)
            )
        } catch (e: MailException) {
            logger.error("Could not send e-mail", e)
        }
    }

    companion object {
        @Suppress("JAVA_CLASS_ON_COMPANION")
        @JvmStatic
        private val logger = LoggerFactory.getLogger(javaClass.enclosingClass)
    }
}