package cloud.dmytrominochkin.mailservice.model

data class User(
    var name: String,
    var age: Int,
    var email: String
)