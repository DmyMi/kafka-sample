package cloud.dmytrominochkin.userservice.model

data class User(
    var name: String,
    var age: Int,
    var email: String
)