plugins {
    id("org.springframework.boot") version "2.5.5" apply false
    kotlin("jvm") version "1.5.31" apply false
    kotlin("plugin.spring") version "1.5.31" apply false
    id("com.google.cloud.tools.jib") version "3.1.4" apply false
}

buildscript {
    repositories {
        mavenCentral()
        maven {
            url = uri("https://plugins.gradle.org/m2/")
        }
    }
    dependencies {
        classpath("io.spring.gradle:dependency-management-plugin:1.0.11.RELEASE")
    }
}

allprojects {
    repositories {
        mavenCentral()
    }
}

configure(subprojects) {

    apply(plugin = "org.springframework.boot")
	apply(plugin = "io.spring.dependency-management")
	apply(plugin = "org.jetbrains.kotlin.jvm")
	apply(plugin = "org.jetbrains.kotlin.plugin.spring")
	apply(plugin = "com.google.cloud.tools.jib")

    dependencies {
        "implementation"("org.springframework.boot:spring-boot-starter-web")
        "implementation"("com.fasterxml.jackson.module:jackson-module-kotlin")
        "implementation"("org.jetbrains.kotlin:kotlin-reflect")
        "implementation"("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
        "implementation"("org.springframework.kafka:spring-kafka")
        "developmentOnly"("org.springframework.boot:spring-boot-devtools")
        "implementation"("org.springframework.boot:spring-boot-starter-actuator")
        "testImplementation"("org.springframework.boot:spring-boot-starter-test")
        "testImplementation"("org.springframework.kafka:spring-kafka-test")
        "annotationProcessor"("org.springframework.boot:spring-boot-configuration-processor")
    }

    group = "cloud.dmytrominochkin"
    version = "0.0.1-SNAPSHOT"

    tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
        kotlinOptions {
            freeCompilerArgs = listOf("-Xjsr305=strict")
            jvmTarget = "11"
        }
    }

    tasks.withType<Test> {
        useJUnitPlatform()
    }

    tasks.withType<JavaCompile>() {
        dependsOn.add(tasks.withType<ProcessResources>())
    }
}

tasks.register("clean", Delete::class) {
    delete(rootProject.buildDir)
}
