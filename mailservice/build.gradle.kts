java.sourceCompatibility = JavaVersion.VERSION_11

dependencies {
	implementation("org.springframework.boot:spring-boot-starter-mail")
	implementation("org.springframework.boot:spring-boot-starter-thymeleaf")
}

jib {
	to {
		image = "registry.gitlab.com/dmymi/kafka-sample/mailservice"
		tags = setOf("latest")
	}
	container {
		ports = listOf("8081")
		jvmFlags = listOf(
			"-server",
			"-Djava.awt.headless=true",
			"-XX:+UseG1GC",
			"-XX:MaxGCPauseMillis=100",
			"-XX:+UseStringDeduplication"
		)
		labels.putAll(
			mapOf(
				"maintainer" to "Dmytro Minochkin <dmytro.minochkin@gmail.com>",
				"org.opencontainers.image.title" to "mailservice",
				"org.opencontainers.image.description" to "An example kafka service",
				"org.opencontainers.image.version" to "$version",
				"org.opencontainers.image.authors" to "Dmytro Minochkin <dmytro.minochkin@gmail.com>",
				"org.opencontainers.image.url" to "https://gitlab.com/DmyMi/kafka-sample/container_registry"
			)
		)
	}
}