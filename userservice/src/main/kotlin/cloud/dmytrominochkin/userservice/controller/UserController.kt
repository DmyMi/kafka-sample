package cloud.dmytrominochkin.userservice.controller

import cloud.dmytrominochkin.userservice.config.KafkaProperties
import cloud.dmytrominochkin.userservice.model.User
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.kafka.core.KafkaTemplate
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController

@RestController
class UserController(
    private val kafkaProperties: KafkaProperties,
    private val kafkaProducer: KafkaTemplate<String, User>
) {

    // curl -v http://localhost:8080/register -H "Content-Type: application/json" -d @user.json
    @PostMapping(value = ["register"], consumes = ["application/json"])
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun register(@RequestBody user: User) {
        if (kafkaProperties.enabled) {
            logger.info("Sending email request: $user")
            kafkaProducer.send(kafkaProperties.topics.userRegistration, user)
        }
    }

    companion object {
        @Suppress("JAVA_CLASS_ON_COMPANION")
        @JvmStatic
        private val logger = LoggerFactory.getLogger(javaClass.enclosingClass)
    }
}