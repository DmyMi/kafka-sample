package cloud.dmytrominochkin.mailservice.model

data class EmailContent(
    var text: String,
    var html: String
)