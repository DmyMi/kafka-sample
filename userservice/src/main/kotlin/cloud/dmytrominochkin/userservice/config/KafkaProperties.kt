package cloud.dmytrominochkin.userservice.config

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.stereotype.Component

@Component
@ConfigurationProperties(prefix = "kafka")
data class KafkaProperties(
    var enabled: Boolean = false,
    var topics: Topics = Topics()
)

data class Topics(
    var userRegistration: String = ""
)