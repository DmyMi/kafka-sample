java.sourceCompatibility = JavaVersion.VERSION_11

jib {
    to {
        image = "registry.gitlab.com/dmymi/kafka-sample/userservice"
        tags = setOf("latest")
    }
    container {
        ports = listOf("8080")
        jvmFlags = listOf(
            "-server",
            "-Djava.awt.headless=true",
            "-XX:+UseG1GC",
            "-XX:MaxGCPauseMillis=100",
            "-XX:+UseStringDeduplication"
        )
        labels.putAll(
            mapOf(
                "maintainer" to "Dmytro Minochkin <dmytro.minochkin@gmail.com>",
                "org.opencontainers.image.title" to "userservice",
                "org.opencontainers.image.description" to "An example kafka service",
                "org.opencontainers.image.version" to "$version",
                "org.opencontainers.image.authors" to "Dmytro Minochkin <dmytro.minochkin@gmail.com>",
                "org.opencontainers.image.url" to "https://gitlab.com/DmyMi/kafka-sample/container_registry"
            )
        )
    }
}