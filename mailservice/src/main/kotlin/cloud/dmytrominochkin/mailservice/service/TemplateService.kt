package cloud.dmytrominochkin.mailservice.service

import cloud.dmytrominochkin.mailservice.model.EmailContent
import cloud.dmytrominochkin.mailservice.model.User
import org.springframework.stereotype.Service
import org.thymeleaf.context.Context
import org.thymeleaf.spring5.SpringTemplateEngine

@Service
class TemplateService(
    private val templateEngine: SpringTemplateEngine
) {
    fun generateEmailContent(user: User): EmailContent {
        val context = Context()
        context.setVariable("name", user.name)
        context.setVariable("age", user.age)
        return EmailContent(
            text = templateEngine.process("registration.txt", context),
            html = templateEngine.process("registration.html", context)
        )
    }
}